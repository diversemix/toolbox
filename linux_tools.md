# tools

## ntp

```bash
ntpq -p
```

## sshfs

sudo apt-get install sshfs
```bash
sudo adduser yourname fuse
logout
mkdir ~/mymount
sshfs username@host:/remotefolder ~/mymount
...
fusermount -u ~/mymount
```

## AcpiTool
```bash
wget http://sourceforge.net/projects/acpitool/files/acpitool/0.5.1/acpitool-0.5.1.tar.gz
cd acpitool-0.5.1
./configure; make; make install
```
Help page is:
```
 Usage: acpitool [option] . . .
 Shows ACPI information from the /proc/acpi filesystem, like battery status,
 temperature, or ac power. Can also suspend your machine (if supported).

   -a, --ac_adapter   AC adapter information
   -A, --Asus         show supported Asus ACPI extensions (LCD brightness level, video out routing DSTD/acpi4asus info)
   -b                 battery status, available batteries only
   -B, --battery      battery status, all info on all battery entries
   -c, --cpu          CPU information (type, speed, cache size, frequency scaling, c-states, . . .)
   -e                 show just about everything
   -f, --fan          show fan status
   -F x               force fan on (x=1) or switch back to auto mode (x=0). (Toshiba only)
   -h, --help         show this help screen
   -j                 eject ultrabay device (Thinkpad only)
   -l x               set LCD brightness level to x, where x is 0..7 (Toshiba and Thinkpad only)
   -m x               switch the mail led on (x=1) or off (x=0) (Asus only)
   -n x               switch the wireless led on (x=1) or off (x=0). (Asus only)
   -o x               set LCD on (x=1) or off (x=0). (Asus only)
   -s, --suspend      suspend to memory (sleep state S3), if supported
   -S                 suspend to disk (sleep state S4), if supported
   -t, --thermal      thermal information, including trip_points
   -T, --Toshiba      show supported Toshiba ACPI extensions (LCD brightness level, video out routing, fan status)
   -v                 be more verbose (more detailed error messages, only usefull combined with other options)
   -V, --version      show application version number and release date
   -w, --wakeup       show wakeup capable devices
   -W x               enable/disable wakeup capable device x. The x can be seen when invoking -w first.
   -z x               set Asus LCD brightness level to x, where x is 0..15 (Asus only).

 If invoked without options, acpitool displays information about available batteries,
 AC adapter and thermal information.

 For more info, type man acpitool at the prompt.

 AcpiTool v0.5.1, released 13-Aug-2009
 Homepage: http://freeunix.dyndns.org:8000/site2/acpitool.shtml
```

## iotop
```
Usage: /usr/sbin/iotop [OPTIONS]

DISK READ and DISK WRITE are the block I/O bandwidth used during the sampling
period. SWAPIN and IO are the percentages of time the thread spent respectively
while swapping in and waiting on I/O more generally. PRIO is the I/O priority at
which the thread is running (set using the ionice command).

Controls: left and right arrows to change the sorting column, r to invert the
sorting order, o to toggle the --only option, p to toggle the --processes
option, a to toggle the --accumulated option, i to change I/O priority, q to
quit, any other key to force a refresh.

Options:
  --version             show program's version number and exit
  -h, --help            show this help message and exit
  -o, --only            only show processes or threads actually doing I/O
  -b, --batch           non-interactive mode
  -n NUM, --iter=NUM    number of iterations before ending [infinite]
  -d SEC, --delay=SEC   delay between iterations [1 second]
  -p PID, --pid=PID     processes/threads to monitor [all]
  -u USER, --user=USER  users to monitor [all]
  -P, --processes       only show processes, not all threads
  -a, --accumulated     show accumulated I/O instead of bandwidth
  -k, --kilobytes       use kilobytes instead of a human friendly unit
  -t, --time            add a timestamp on each line (implies --batch)
  -q, --quiet           suppress some lines of header (implies --batch)
```

## powertop
```
Usage: powertop [OPTIONS]

--debug                  run in "debug" mode
--version                print version information
--calibrate              runs powertop in calibration mode
--extech[=devnode]       uses an Extech Power Analyzer for measurements
--html[=FILENAME]        generate a html report
--csv[=FILENAME]         generate a csv report
--time[=seconds]         generate a report for 'x' seconds
--iteration[=iterations] number of times to run each test
--workload[=workload]    file to execute for workload
--quiet                  suppress stderr output
--help                   print this help menu

For more help please refer to the README
```

## netdiscover
