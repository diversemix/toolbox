# Monitoring

## OMD & check_mk

### Quickstart
 * Install the Open Monitoring distribution on the server: 

```
wget http://files.omdistro.org/releases/centos_rhel/omd-1.20.rhel7.x86_64.rpm
yum install ./omd-1.20.rhel7.x86_64.rpm
```

 * Create the site on the server: ```omd create <site_name>```
 * Start the site: ```omd start <site_name>```
 * You might have to tell selinux to allow httpd: ```semanage permissive -a httpd_t```
 * Now you should be able to browse to http://<server>/<site_name> login as "omdadmin" with password "omd"

### Configuration
 * Switch to the user with the name of the site: ```runuser -u <site_name> /bin/bash```
 * Add servers/processes to check in the file: ```~/etc/check_mk/main.mk```
 * Note that rules set up via WATO are in: ```~/etc/check_mk/conf.d/wato/rules.mk```
 * Whenever anything is changed update the inventory: ```bin/cmk -I``` followed by a reload: ```bin/cmk -R```

#### Remote up Agent
 * On the omd server change to the folder: ```/omd/versions/default/share/check_mk/agents/```
 * Assuming the server you wish to monitor is called ```<server_x>```

```
scp check_mk_agent.linux <server_x>:/usr/bin/check_mk_agent
scp xinetd.conf <server_x>:/etc/xinetd.d/check_mk
ssh <server_x>
systemctl restart xinetd
``` 

 * Before the monitoring will work you will need to change the ```only_from``` line in ```/etc/xinetd.d/check_mk``` to add the host's ip address, for example if its 192.168.0.10:

```
only_from      = 192.168.0.10 127.0.0.1
```
 * The status is streamed from port 6556 to you can test all is fine by: ```telnet <server_x> 6556``` You should see a stream of data fly-by.
 
 
### Setting up alerts
This isn't very intuative but makes sense after you've been through it. Details here:

http://mathias-kettner.de/checkmk_flexible_notifications.html

### Configuration Backup and Restore
In order to save the configuration it is recommended that it is done via backup / restore. This create a tar of all the configuration folders
https://mathias-kettner.de/checkmk_backup.html

### List of possible checks
http://mathias-kettner.de/checkmk_checks.html

#### Checking ports

See: http://tldp.org/LDP/abs/html/devref1.html

A surprise I found out recently is that Bash natively supports tcp connections as file descriptors. To use:

```
exec 6<>/dev/tcp/ip.addr.of.server/445
echo -e "GET / HTTP/1.0\n" >&6
cat <&6
```

I'm using 6 as the file descriptor because 0,1,2 are stdin, stdout, and stderr. 5 is sometimes used by Bash for child processes, so 3,4,6,7,8, and 9 should be safe.

As per the comment below, to test for listening on a local server in a script:

```
exec 6<>/dev/tcp/127.0.0.1/445 || echo "No one is listening!"
exec 6>&- # close output connection
exec 6<&- # close input connection
```

To determine if someone is listening, attempt to connect by loopback. If it fails, then the port is closed or we aren't allowed access. Afterwards, close the connection.

Modify this for your use case, such as sending an email, exiting the script on failure, or starting the required service.


### Debuging 
Example: Look for the Filecount response from server-X
```bash
bin/cmk -d server-X|grep Filecount
```

Example: Run all checks for server-X
```bash
bin/cmk -nv server-X
```

----

## cactii