# tcp server

Below is some code that acts as a TCPServer on port 80, so like a web server. It is very dump and has fixed responses that can be found in the 'ResponseMap' 
```python
#! /usr/bin/env python

import sys
import SocketServer
from mapper import ResponseMap

class MyTCPHandler(SocketServer.BaseRequestHandler):
    """
    The RequestHandler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        # self.request is the TCP socket connected to the client
        map = ResponseMap()
        while True:
            peer = self.client_address[0]
            message = self.request.recv(100)
            response = ''
            print "{} wrote:n".format(peer, message)
            print message
            response = map.getResponse(message)
            print "nresponded:n%sn" % (response)
            sys.stdout.flush()
            self.request.sendall(response)

    print "Client connection ended"

if __name__ == "__main__":
    HOST, PORT = "www.myserver.com", 80

    print "Starting server %s on %d" % (HOST, PORT)
    server = SocketServer.TCPServer((HOST, PORT), MyTCPHandler)

    # Activate the server; this will keep running until you
    # interrupt the program with Ctrl-C
    server.serve_forever()
```
