# toolbox
A toolbox of software, scripts and knowledge.

## philosophy
 * [home](home.md)
 * [security](security.md)
 
## development environment
* [linux tools](linux_tools.md)
* [links](links.md)
* [bash](bash.md)
* [communication](communication.md)

## systems & services
* [principles](principles.md)
* [infrastructure](infrastructure.md)
* [deployment](deployment.md)
* [monitoring](monitoring.md)

## applications
 * [mobile](mobile.md)
 * [javacard](javacard.md)
 * [windows](windows.md)

## languages
 * [nodejs](node.md)
 * [python](python.md)
 * [C](c.md)
 * [C#](csharp.md)
 * [java](java.md)