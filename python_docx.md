# opening docx files

Below is my little python application for parsing a Microsoft docx file. It turns out that the docx file is a zipped xml file so the code is quite simple.

```python
"""
    http://docs.python.org/2/library/xml.etree.elementtree.html
"""
import zipfile
import re
import xml.etree.ElementTree as ET

def clean_string(string):
    regex = '({http://.*?})'
    if re.search(regex, string):
        return re.sub(regex, '', string)
    else:
        return string

def print_node(root, spacing=''):

    for child in root:
        tag = clean_string(str(child.tag))
        print spacing, "<", tag, "> ", clean_string(str(child.attrib))
        try:
            if tag == 't':
                print child.text.encode('utf8')
        except UnicodeEncodeError as uee:
            print " *ERROR* "

        print_node(child, spacing + '    ')

def dump_text_node(root):
    for child in root:
        tag = clean_string(str(child.tag))
        try:
            if tag == 't':
                print child.text.encode('utf8')
        except UnicodeEncodeError as uee:
            print " *ERROR* "

        dump_text_node(child)

def process_file(filename):
    doc_as_zip = zipfile.ZipFile(filename)
    xml_string = doc_as_zip.read("word/document.xml")
    root = ET.fromstring(xml_string)
    print_node(root)
    #dump_text_node(root)
    doc_as_zip.close()

process_file("C:d.docx")

```
