# general
http://devdocs.io/
https://www.codeschool.com/

# REST
http://www.restapitutorial.com/lessons/httpmethods.html
 
# python
## sphinx
http://stackoverflow.com/questions/24335079/how-do-i-increase-the-fixed-width-font-size-in-sphinx-restructured-text
https://pythonhosted.org/an_example_pypi_project/sphinx.html
```python
sudo pip install sphinx 
sphinx-quickstart
```

## cython

Cython (http://cython.org) is a language that makes writing C extensions for the Python language as easy as Python itself. Cython is based on the well-known Pyrex, but supports more cutting edge functionality and optimizations.

https://github.com/cython/cython

## django
https://docs.djangoproject.com/en/dev/ref/templates/api/
https://docs.djangoproject.com/en/dev/topics/db/queries/

## daemons

https://pypi.python.org/pypi/daemonocle/
http://www.jejik.com/articles/2007/02/a_simple_unix_linux_daemon_in_python/

## SQLAlchemy
http://docs.sqlalchemy.org/en/rel_0_9/orm/tutorial.html

## celery
http://stackoverflow.com/questions/20164688/celery-and-django-simple-example
http://docs.celeryproject.org/en/latest/django/first-steps-with-django.html

# design

http://www.creativebloq.com/typography/free-web-fonts-1131610
http://www.google.com/fonts/
http://mashable.com/2013/10/09/web-design-inspiration-2/

## icons
http://www.wpzoom.com/wpzoom/500-free-icons-wpzoom-social-networking-icon-set/

# Open Stack
https://www.youtube.com/watch?v=bCsw2kkIWyw

# erlang

Rebar: http://zotonic.com/docs/0.9/manuals/cookbook/justenough-otp1.html
Erlang Course: http://www.erlang.org/course/course.html
Samples: http://www1.erlang.org/examples/small_examples/index.html

# databases
## pyMySQL
Primarily for circumventing licencing issues.

https://github.com/PyMySQL/PyMySQL/blob/master/example.py
https://pypi.python.org/pypi/PyMySQL
