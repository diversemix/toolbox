
##Adding a route
```bash
route add -net 1.1.1.0/24 dev bond1:db
route add -net 2.2.2.0/24 gw 2.2.2.100
```

##Adding an alias
```bash
ifconfig bond0:myalias 192.168.x.x/24
```

