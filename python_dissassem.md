# python disassembler

This is an small piece of code to show how to use the python disassembler:

```python
import dis
def f():
    a = 1 + 2

dis.dis(f)
```

