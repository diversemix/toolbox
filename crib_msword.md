| Keystroke| Description |
| ------   | --------   |
| ALT+CTRL+1| Apply Heading 1 to the selected text| 
| ALT+CTRL+1| Apply Heading 1 to the selected text| 
| ALT+CTRL+2          |     Apply Heading 2 to the selected text  | 
| ALT+CTRL+S          |     Splits the Document                   | 
| ALT+F7              |     Moves to the Next Misspelling         | 
| ALT+F8              |     Inserts Macros                        | 
| ALT+R               |     Displays the Review tab               | 
| ALT+SHIFT+BACKSPACE |     Redo| 
| ALT+SHIFT+F7    |     Dictionary| 
| ALT+SHIFT+K    |     Mail Merge Check| 
| ALT+SHIFT+R    |     Header Footer Link| 
| ALT+SHIFT+T    |     Time Field| 
| ALT+T+A        |     Autocurrect| 
| CTRL +  V    |     Paste copied text| 
| CTRL + B    |     Bold Text| 
| CTRL + C    |     Copy text selection| 
| CTRL + I    |     Italic Text| 
| CTRL + L    |     Align selected text to the left| 
| CTRL + U    |     Underline Text| 
| CTRL + X    |     Cut the selected text| 
| CTRL + Z    |     Undo| 
| CTRL+]        |     Grow Font one point| 
| CTRL+E        |     Align selected text to the centre| 
| CTRL+END    |     Navigate to end of the Document| 
| CTRL+H        |     Find and Replace word or sentence| 
| CTRL+J        |     Justifies Paragraph| 
| CTRL+K        |     Add hyperlink to the selected text| 
| CTRL+M        |     Indentation of  the selected text| 
| CTRL+P        |     Print the document| 
| CTRL+PAGE DOWN    |     Browse Next| 
| CTRL+SHIFT+,    |     Reduce Font size for selected text| 
| CTRL+SHIFT+.    |     Increase Fontsise for selected text| 
| CTRL+SHIFT+A    |     converts the selected text to capital letters or vice versa| 
| CTRL+SHIFT+C    |     Copy Format| 
| CTRL+SHIFT+D    |     Double Underline| 
| CTRL+SHIFT+ENTER|         Column Break| 
| CTRL+SHIFT+F    |     Displays the Font dialog box.| 
| CTRL+SHIFT+F12    |     Also launches Print| 
| CTRL+SHIFT+F5    |     Bookmark| 
| CTRL+SHIFT+G    |     Displays the Word Count dialog box.| 
| CTRL+SHIFT+K    |     Small Caps| 
| CTRL+SHIFT+L    |     Applies Bullets| 
| CTRL+SHIFT+M    |     Unindent the selection| 
| CTRL+SHIFT+P    |     Font size select| 
| CTRL+SHIFT+S    |     Displays the Apply Styles task pane.| 
| CTRL+SHIFT+S    |     Style| 
| END        |     End of line| 
| F1        |     Help window| 
| F10        |     Menu Mode| 
| F12        |     Save As| 
| F5        |     Goto page number| 
| F7         |     Spelling and grammar check| 
| SHIFT+F5    |     Go Back to previous state| 
| ALT+CTRL+Z    |     Go Back to previous state| 
| SHIFT+F7    |     Thesaurus |