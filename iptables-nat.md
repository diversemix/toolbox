# iptables

## docker
sudo iptables -t nat -A  DOCKER -p tcp --dport 3000 -j DNAT --to-destination 172.17.1.61:3000

## other
The mystical art of "iptables" can be a little overwhelming at first but its pretty straight forward once you know what you are looking for. First start by issuing the following command to list your rules:

```bash
iptables -L  
``` 

Blah Blah Blah Use a script to test before you do a final save.

```bash
#!/bin/sh

############################################################
# Set up some vars...
############################################################

SERVER_IP="5.5.5.1"
INCOMING_IP="0/0"

############################################################
# Reset iptables
############################################################

service iptables restart
iptables -F
iptables -X

############################################################
# Default Policies
############################################################
iptables -P INPUT DROP
iptables -P OUTPUT DROP
iptables -P FORWARD DROP

############################################################
# Allow loopback
############################################################
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT
 
############################################################
# Allow SSH
############################################################
iptables -A INPUT -p tcp -s $INCOMING_IP -d $SERVER_IP --sport 513:65535 --dport 22 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -p tcp -s $SERVER_IP -d $INCOMING_IP --sport 22 --dport 513:65535 -m state --state ESTABLISHED -j ACCEPT

############################################################
# Allow MySQL
############################################################
iptables -A INPUT -p tcp -s $INCOMING_IP -d $SERVER_IP --sport 513:65535 --dport 3306 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -p tcp -s $SERVER_IP -d $INCOMING_IP --sport 3306 --dport 513:65535 -m state --state ESTABLISHED -j ACCEPT

############################################################
# Allow Server 11
############################################################
SERVER_11="1.1.1.11"
MY_INTERFACE="2.2.2.1"
iptables -A INPUT -i eth2 -s $SERVER_11 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -o eth2 -s $MY_INTERFACE -d $SERVER_11 -m state --state NEW,ESTABLISHED -j ACCEPT

############################################################
# Forward to 9.9.9.9
############################################################

DST_SERVER="9.9.9.9"
FROM_IP="0/0"

sysctl -w net.ipv4.ip_forward=1

iptables -t nat -A PREROUTING -p tcp -i eth1 -d $SERVER_IP --dport 3306 -j DNAT --to $DST_SERVER:3306
iptables -t nat -A PREROUTING -p tcp -i eth1 -d $SERVER_IP --dport 52000 -j DNAT --to $DST_SERVER:52000
iptables -t nat -A POSTROUTING -p tcp -o eth2 -j SNAT --to-source $MY_INTERFACE
iptables -A FORWARD -p tcp -d $DST_SERVER --dport 3306 -j ACCEPT
iptables -A FORWARD -p tcp -d $DST_SERVER --dport 52000 -j ACCEPT
iptables -A FORWARD -t filter -o eth1 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
iptables -A FORWARD -t filter -i eth1 -m state --state ESTABLISHED,RELATED -j ACCEPT

############################################################
# Logging Chain
############################################################
iptables -N LOGGING
iptables -A FORWARD -j LOGGING
iptables -A LOGGING -m limit --limit 5/second -j LOG --log-level 7 --log-prefix "Dropped by firewall: "
iptables -A LOGGING -j DROP

############################################################
# Finally drop everything
############################################################
#iptables -A INPUT -j LOG
#iptables -A OUTPUT -j LOG
iptables -A FORWARD -j LOG

iptables -A INPUT -j DROP
iptables -A OUTPUT -j DROP
iptables -A FORWARD -j DROP

```
