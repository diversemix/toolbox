# open EAP file

Opening an EAP (Enterprise Architect) file with python could not be easier. The file is basically a Microsoft Database file (mdb) and can be opened and queried using pyodbc, see the following code snippet...

```python
import pyodbc

cnxn = pyodbc.connect('DRIVER={Microsoft Access Driver (*.mdb)};DBQ=C:\Test.eap')
cursor = cnxn.cursor()

cursor.execute(" SELECT Name FROM t_diagram;")

rows = cursor.fetchall()
for row in rows:
    print row.Name

```
