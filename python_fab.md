# fabric
Test installation of pycrypto with:
```python
python -c "from Crypto.Cipher import AES"
```
Test the installation of fabric with:
```python
python -c "import fabric.api”
```
| Package | Source |
| ---- | ---- |
| Fabric-1.7.0.win32.msi | 	http://fabfile.org |
| paramiko-1.11.0.win32.msi | 	http://www.lag.net/paramiko/ |
| pycrypto-2.6.win32-py2.7.msi | 	https://www.dlitz.net/software/pycrypto/ |
| python-2.7.5.msi | 	http://www.python.org/ |
| setuptools-1.1.4.win32.msi | 	http://pythonhosted.org/setuptools/ |
