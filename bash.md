# bash

## scripting

### variables
```bash
#!/bin/bash
 OF=/var/my-backup-$(date +%Y%m%d).tgz
 tar -cZf $OF /home/me/
```
```bash
#!/bin/bash
HELLO=Hello
function hello {
     local HELLO=World
     echo $HELLO
}
echo $HELLO
hello
echo $HELLO
```

### if
```bash
#!/bin/bash
T1="foo"
T2="bar"
if [ "$T1" = "$T2" ]; then
    echo expression evaluated as true
else
    echo expression evaluated as false
fi
```

### for
```bash
#!/bin/bash
for i in $( ls ); do
    echo item: $i
done
```
```bash
#!/bin/bash
for i in `seq 1 100`;
do
    echo $i
done
```

### loops
```bash
#!/bin/bash
COUNTER=0
while [ $COUNTER -lt 10 ]; do
    echo The counter is $COUNTER
    let COUNTER=COUNTER+1
done
```
```bash
#!/bin/bash
COUNTER=20
until [ $COUNTER -lt 10 ]; do
    echo COUNTER $COUNTER
    let COUNTER-=1
done
```

### debug
```bash
#!/bin/bash -x
```

## building

This can can be highly frustrating, but try this command to configure:

```bash
CFLAGS="-m32" LDFLAGS="-m32" ./configure --target=x86
```

## processes
Below is example code to stop processes with a particular name.

```bash
#!/bin/bash
TAG=<process-name-group>

how_many() {
    n=`ps aux|grep $TAG|grep -v grep|wc -l`
}

how_many
echo "Found $n processes running."

if [ $n -gt 0 ]
then
        ps aux | grep $TAG | grep -v grep | awk -F ' ' '{ print $2 }'| xargs kill -9
else
        echo "No process running!"
fi

for ((  ; n>0 ; ))
do
        how_many
        echo "Waiting for $n processes to stop..."
        sleep 1
done

```

## curl

Simplest usage:
```bash
curl http://here.com
```
Usage with POST:
```bash
curl -X POST -d "username=test&password=test" http://here.com
```
Additional arguments to get any redirects (HTTP 302):
```bash
 -L -w "Last URL: %{url_effective}"
```
Usage with Cookies, add the following arguments:
```bash
-c cookie.txt -b cookie.txt
```
Parse the output for the TOKEN
```bash
grep csrf cookie.txt|cut -f7
```
Now use this token to format your next request...
```bash
 curl -X POST -c cookie.txt -b cookie.txt -d "username=tesr&password=test&csrfmiddlewaretoken=<<TOKEN_FROM_COOKIE_FILE>>" http://here.com
```