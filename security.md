# browser

As part of the SSL/TLS  protocol when setting up a secure connection between the server and the browser there is some negotiating done as to what level of security to use. You can test your browser SSL/TLS options by visiting: https://www.howsmyssl.com/ HowsMySSL.com. Ther are other tools you can use to determine your security level,  "sslscan" and "sslyze.py" - and of course Wireshark.

There are different levels of the protocol a good starting point is here.

## securing firefox

Open a tab to about:config. Filter for tls.version and set the min/max appropriately. Here's the breakdown of the meaning of the values. http://kb.mozillazine.org/Security.tls.version.%2a

You can also mess with the accepted cipher suites via options (filter about:config for: security.ssl3).

# signing

Download White Paper Using separate key pairs for signing and encryption
http://en.wikipedia.org/wiki/Digital_signature#Using_separate_key_pairs_for_signing_and_encryption

    In several countries, a digital signature has a status somewhat like that of a traditional pen and paper signature, like in the EU digital signature legislation. Generally, these provisions mean that anything digitally signed legally binds the signer of the document to the terms therein. For that reason, it is often thought best to use separate key pairs for encrypting and signing. Using the encryption key pair, a person can engage in an encrypted conversation (e.g., regarding a real estate transaction), but the encryption does not legally sign every message he sends. Only when both parties come to an agreement do they sign a contract with their signing keys, and only then are they legally bound by the terms of a specific document. After signing, the document can be sent over the encrypted link. If a signing key is lost or compromised, it can be revoked to mitigate any future transactions. If an encryption key is lost, a backup or key escrow should be utilized to continue viewing encrypted content. Signing keys should never be backed up or escrowed.

Taken from a forum: Signing before Encryption and Signing after Encryption
http://seclists.org/basics/2006/Mar/273

    Encrypting with a private key is equivalent to signing it (and is actually how signing usually takes place). So in your scenario (using solely asymmetric keys): Alice encrypts the message first with Bob's public key and then with Alice's private key. Mallory decrypts the message with Alice's public key (since the public key is the inverse of the private key) and is left with just the message encrypted to Bob's public key. The attack proceeds as before. Signing a message usually consists of encrypting a secure hash of the message with your private key. Using a hash (even one such as MD5 or SHA1) does not add to the security at all, but instead weakens the security of the signature. The only reason for the hash is efficiency. As already noted in this thread, asymmetric encryption is slow, so by only signing the hash, it is much faster. Also, if instead of signing the hash you were to sign the message, you'd double the size of the message that needs to be transmitted. Using hashes introduces all of the risks associated with hash collisions, a risk that would never occur if you were to sign the message directly. (Please correct me if I'm wrong. It's been a while since I've worked on the underlying math and strategies.)

# stuff learnt about encryption

 - PGP and GnuPG - these are just for file encryption.
 - encfs - does not ecrypt the filenames, each file is done on a file-per-file bases making it useful for the cloud.

 - cryptosetup and LUKS - best sort of encryption
 - need to use the password manager more 
 - Rainbow tables (see delicious)


